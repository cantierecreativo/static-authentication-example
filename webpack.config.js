const path = require('path');

module.exports = {
  entry: {
    application: './source/javascripts/application.js',
  },
  resolve: {
    modules: [
      path.join(__dirname, 'source/javascripts'),
      "node_modules"
    ]
  },
  output: {
    path: path.resolve(__dirname, '.tmp/dist'),
    filename: 'javascripts/[name].js',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  },
  plugins: [ ]
};
