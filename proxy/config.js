// Listen port
exports.port = process.env.PORT || 3000;
// Log request message into console
exports.enable_logging = true;
// The lenght of time we'll wait for a proxy server to respond before timing out.
exports.proxy_request_timeout_ms = 10000;
// Forwarding settings
//Check if the url to be forward is match the pattern "check",if so forward it to "url".
exports.forwards = [
  {
      check: /^\/.netlify\/identity/,
      url: "https://static-authentication-example.netlify.com/.netlify/identity"
  }
];
// If none of the "forwards" is matched,this become the forwarding address.
// http , https
exports.else_forward_url = "http://127.0.0.1:4567";
