# Middleman, React, Netlify Identity example website with authentication

This repository contains a Middleman website with basic Netlify Identity authentication implementation written in React.

It uses the customisable GoTrue JS library.

The site is deployed on Netlify, and can be seen at this URL: https://static-authentication-example.netlify.com/#/

The tutorial for the realisation of this project can be found 

## Usage

```
# install dependencies
bundle install
yarn
# start Middleman site
bundle exec middleman
# build Middleman site
bundle exec middleman build
```

Run the proxy by navigating to the proxy folder

```
cd proxy
yarn start
```

Navigate to http://localhost:3000

## Website preview

![Website screenshot](https://raw.githubusercontent.com/datocms/jekyll-example/master/screenshot.png)
