import auth from "../gotrue";

export function signin(values) {
  return auth.login(values.email, values.password, true)
}

export function signup(values) {
  return auth.signup(values.email, values.password, {})
}
