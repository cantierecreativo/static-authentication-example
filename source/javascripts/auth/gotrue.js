import GoTrue from "gotrue-js";
import isEqual from 'lodash-es/isEqual';

const gotrue = new GoTrue({
  APIUrl: "/.netlify/identity",
  audience: "",
  setCookie: true
});

let listeners = [];
let currentUser;

gotrue.subscribe = (cb) => {
  listeners.push(cb);

  return function unsubscribe() {
    listeners = listeners.filter(c => c !== cb);
  }
}

function checkCurrentUser() {
  const newCurrentUser = gotrue.currentUser();

  if (newCurrentUser && !newCurrentUser.user_metadata) {
    setTimeout(checkCurrentUser, 100);
    return;
  }

  let promise = Promise.resolve(newCurrentUser);

  if (newCurrentUser) {
    promise = newCurrentUser.jwt().then(() => newCurrentUser);
  }

  promise.then((newCurrentUser) => {
    if (!isEqual(newCurrentUser, currentUser)) {
      currentUser = newCurrentUser;
      listeners.forEach(cb => cb(newCurrentUser));
    }

    setTimeout(checkCurrentUser, 100);
  })
}

setTimeout(checkCurrentUser, 100);

export default gotrue;
