import React from "react";
import { HashRouter, Route } from "react-router-dom";
import AuthForm from "./AuthForm";
import AuthBar from "./AuthBar";

export default class App extends React.Component {
  render() {
    return (
      <HashRouter>
        <div>
          {
            document.getElementById('auth-navigation') &&
              <AuthBar className="site-header__user" domNodeId="auth-navigation" />
          }
          <Route exact path="/signin" component={ () => <AuthForm signinIn={true}/> }/>
          <Route exact path="/signup" component={ () => <AuthForm signinIn={false}/> }/>
        </div>
      </HashRouter>
    );
  }
}
