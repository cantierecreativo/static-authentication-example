import React from "react";
import ReactDOM from "react-dom";
import { Link } from "react-router-dom";

export default class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.el = document.createElement('div');
  }

  componentDidMount() {
    // The portal element is inserted in the DOM tree after
    // the Modal's children are mounted, meaning that children
    // will be mounted on a detached DOM node. If a child
    // component requires to be attached to the DOM tree
    // immediately when mounted, for example to measure a
    // DOM node, or uses 'autoFocus' in a descendant, add
    // state to Modal and only render the children when Modal
    // is inserted in the DOM tree.
    document.querySelector("body").appendChild(this.el);
  }

  componentWillUnmount() {
    document.querySelector("body").removeChild(this.el);
  }

  render() {
    const { title, children, handleClose} = this.props;

    return ReactDOM.createPortal(
      <div className="dialog">
        <div className="dialog__frame">
          <Link to="/" className="dialog__close" onClick={handleClose && handleClose.bind(this)}>
            <span>Close</span>
          </Link>
          <div className="dialog__body">
            <div className="grid--center">
              <div className="grid__item width-7-12">
                <div className="align--center">
                  <span className="footer__title">{title}</span>
                </div>
              </div>
            </div>
            <hr className="space--both-2" />
            {this.props.children}
          </div>
        </div>
      </div>,
      this.el,
    );
  }
}
